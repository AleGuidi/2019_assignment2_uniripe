# 2019_assignment2_UniRipe  

**Membri del gruppo**  
*  Alessandro Guidi 808865 
*  Vittorio Maggio 817034  
* Stefano Zuccarella 816482  
 

Tutti i membri hanno partecipato attivamente alla stesura della relazione.  
  
**Applicazione**  
L'applicazione sulla quale è stata svolta l'analisi dei requisiti è UniRipe, la quale permette a studenti universitari di offrire e/o ricevere ripetizioni per specifici esami universitari delle università Milanesi.  
  
**Note**  
Link del questionario: https://docs.google.com/forms/d/e/1FAIpQLSe9WntBSw5A8cwN3Xeqt4jHTU2EuXDsnN4n9xv3tMzsUXdMuA/viewform?usp=sf_link
Link del mock-up: https://marvelapp.com/a03g931

Per vedere i dati ottenuti da ciascun utente dal questionario, si apra il relativo file xlsx per aprirlo con Excel oppure il file odt per aprirlo con LibreOffice.
Di seguito è riportata la separazione delle domande nei file contenenti i dati:
- ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `Domande riguardanti le informazioni generiche`
- ![#ffff00](https://placehold.it/15/ffff00/000000?text=+) `Domande per cambiare le macro-sezioni`
- ![#ff6600](https://placehold.it/15/ff6600/000000?text=+) `Domande effettuate agli insegnanti con esperienza`
- ![#9900cc](https://placehold.it/15/9900cc/000000?text=+) `Domande effettuate agli insegnanti senza esperienza`
- ![#009933](https://placehold.it/15/009933/000000?text=+) `Domande effettuate agli studenti con esperienza`
- ![#3333cc](https://placehold.it/15/3333cc/000000?text=+) `Domande effettuate agli studenti senza esperienza`
- ![#ff00ff](https://placehold.it/15/ff00ff/000000?text=+) `Domande effettuate agli interessati a provare il prototipo`